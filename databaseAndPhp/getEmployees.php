<?php
require 'db_connection.php';
$httpResponse = array();

if($_SERVER['REQUEST_METHOD']=='POST'){

  $lotId = $_POST['lot_id'];
  $attenDate = $_POST['atten_date'];

  //fetch employees from marked attendance record
  $getEmployees1 = mysql_query("SELECT * FROM employee_biodata,employee_category,employee_attendance WHERE employee_biodata.lot_id='$lotId' AND employee_biodata.lot_id=employee_attendance.lot_id AND employee_biodata.category_id = employee_category.category_id AND employee_biodata.user_name=employee_attendance.employee_id AND employee_attendance.attendance_date='$attenDate' ORDER BY employee_biodata.biodata_id ASC");

  $numEmployees= mysql_num_rows($getEmployees1);

	if($numEmployees>0){

    //fetch employee record and store in array
    while($record = mysql_fetch_array($getEmployees1)){
      array_push($httpResponse, array(
        'empId'=>$record[0],
        'fName'=>$record[1],
        'lName'=>$record[2],
        'email'=>$record[3],
        'phone'=>$record[4],
        'category'=>$record[11],
        'attd_status'=>$record[13]));
    }
    echo json_encode(array("reqStatus"=>"ok","result" => $httpResponse));

	}
  else{//if previous attendance record is not found, fetch fresh from employee biodata
      $getEmployees2 = mysql_query("SELECT * FROM employee_biodata,employee_category WHERE employee_biodata.lot_id='$lotId' AND employee_biodata.category_id = employee_category.category_id ORDER BY biodata_id ASC");

      if(mysql_num_rows($getEmployees2)>0){

        while($record = mysql_fetch_array($getEmployees2)){
          array_push($httpResponse, array(
            'empId'=>$record[0],
            'fName'=>$record[1],
            'lName'=>$record[2],
            'email'=>$record[3],
            'phone'=>$record[4],
            'category'=>$record[11],
            'attd_status'=>'A'));
        }
        echo json_encode(array("reqStatus"=>"ok","result" => $httpResponse));
      }
      else{
        echo json_encode(array("reqStatus"=>"error","result" => "No employee record found on the LOT"));
    	}
  }
}

?>

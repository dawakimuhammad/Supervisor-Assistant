<?php
require 'db_connection.php';
$httpResponse = array();

if($_SERVER['REQUEST_METHOD']=='POST'){

	$optionsToFetch = $_POST['fetch_id'];

	if($optionsToFetch=="lot_id"){

		$getRecords = mysql_query("SELECT * FROM lot_info ORDER BY lot_name ASC");
		$numRecordsFound= mysql_num_rows($getRecords);

		if($numRecordsFound>0){
	    //fetch all LOT name and  store in array

	    while($record = mysql_fetch_array($getRecords)){
	      array_push($httpResponse, array(
	        'lotId'=>$record[0],
					'lotName'=>$record[1]
				));
	    }
	    echo json_encode(array("reqStatus"=>"ok","result" => $httpResponse));

		}
		else{
	    echo json_encode(array("reqStatus"=>"error","result" => "No LOT found"));
		}

	}
	elseif ($optionsToFetch=="category_id") {
		$getRecords = mysql_query("SELECT * FROM employee_category ORDER BY category_name ASC");
		$numRecordsFound= mysql_num_rows($getRecords);

		if($numRecordsFound>0){
	    //fetch all category name and  store in array

	    while($record = mysql_fetch_array($getRecords)){
	      array_push($httpResponse, array(
	        'categoryId'=>$record[0],
					'categoryName'=>$record[1]
				));
	    }
	    echo json_encode(array("reqStatus"=>"ok","result" => $httpResponse));

		}
		else{
	    echo json_encode(array("reqStatus"=>"error","result" => "No Category found"));
		}

	}
	elseif ($optionsToFetch=="equipment_id") {
		$getRecords = mysql_query("SELECT * FROM equipment_table ORDER BY equipment_name ASC");
		$numRecordsFound= mysql_num_rows($getRecords);

		if($numRecordsFound>0){
	    //fetch all equipment name and  store in array

	    while($record = mysql_fetch_array($getRecords)){
	      array_push($httpResponse, array(
	        'equipmentId'=>$record[0],
					'equipmentName'=>$record[1]
				));
	    }
	    echo json_encode(array("reqStatus"=>"ok","result" => $httpResponse));

		}
		else{
	    echo json_encode(array("reqStatus"=>"error","result" => "No equipment found"));
		}

	}

}

?>

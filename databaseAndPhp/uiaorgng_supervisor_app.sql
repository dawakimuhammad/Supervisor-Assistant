-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 14, 2018 at 03:10 PM
-- Server version: 5.6.40
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uiaorgng_supervisor_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee_attendance`
--

CREATE TABLE `employee_attendance` (
  `attendance_id` int(11) NOT NULL,
  `attendance_status` enum('P','A','S','O') NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `lot_id` varchar(20) NOT NULL,
  `supervisor_id` varchar(100) NOT NULL,
  `attendance_date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_attendance`
--

INSERT INTO `employee_attendance` (`attendance_id`, `attendance_status`, `employee_id`, `lot_id`, `supervisor_id`, `attendance_date`) VALUES
(1, 'A', '4', '1', 'elnasabdallah@gmail.com', '2018-9-10'),
(2, 'A', '13', '1', 'elnasabdallah@gmail.com', '2018-9-10'),
(3, 'A', '6', '1', 'elnasabdallah@gmail.com', '2018-9-10'),
(4, 'A', '11', '1', 'elnasabdallah@gmail.com', '2018-9-10'),
(5, 'P', '1', '1', 'elnasabdallah@gmail.com', '2018-9-10'),
(6, 'P', '2', '1', 'elnasabdallah@gmail.com', '2018-9-10'),
(7, 'P', '3', '1', 'elnasabdallah@gmail.com', '2018-9-10'),
(15, 'P', '4', '1', 'elnasabdallah@gmail.com', '2018-9-11'),
(16, 'P', '4', '1', 'elnasabdallah@gmail.com', '2018-9-12'),
(17, 'A', '13', '1', 'elnasabdallah@gmail.com', '2018-9-11'),
(18, 'A', '13', '1', 'elnasabdallah@gmail.com', '2018-9-12'),
(19, 'P', '6', '1', 'elnasabdallah@gmail.com', '2018-9-11'),
(20, 'A', '6', '1', 'elnasabdallah@gmail.com', '2018-9-12'),
(21, 'P', '11', '1', 'elnasabdallah@gmail.com', '2018-9-11'),
(22, 'A', '11', '1', 'elnasabdallah@gmail.com', '2018-9-12'),
(23, 'P', '1', '1', 'elnasabdallah@gmail.com', '2018-9-11'),
(24, 'P', '1', '1', 'elnasabdallah@gmail.com', '2018-9-12'),
(25, 'P', '2', '1', 'elnasabdallah@gmail.com', '2018-9-11'),
(26, 'P', '2', '1', 'elnasabdallah@gmail.com', '2018-9-12'),
(27, 'P', '3', '1', 'elnasabdallah@gmail.com', '2018-9-11'),
(28, 'P', '3', '1', 'elnasabdallah@gmail.com', '2018-9-12');

-- --------------------------------------------------------

--
-- Table structure for table `employee_biodata`
--

CREATE TABLE `employee_biodata` (
  `biodata_id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `lot_id` varchar(20) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `category_id` varchar(20) NOT NULL,
  `account_status` enum('1','0') NOT NULL,
  `date_added` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_biodata`
--

INSERT INTO `employee_biodata` (`biodata_id`, `firstname`, `lastname`, `email`, `phone`, `lot_id`, `user_name`, `category_id`, `account_status`, `date_added`) VALUES
(1, 'Muhammad', 'Dawaki', 'dawaki@live.com', '07038955776', '1', 'dawaki@live.com', '1', '1', '2018-8-11'),
(2, 'Muhammad', 'Nasir Abdullahi', 'elnasabdallah@gmail.com', '07068218087', '1', 'elnasabdallah@gmail.com', '4', '1', '2018-08-12'),
(3, 'Sadiq', 'Abubakar', 'abmusadeeq@gmail.com', '07012345678', '1', 'abmusadeeq@gmail.com', '2', '1', '2018-08-12'),
(4, 'Ismail', 'Muhammad', 'samailaadamumuazu@gmail.com', '07012345678', '1', 'samailaadamumuazu@gmail.com', '2', '1', '2018-08-12'),
(5, 'Hafsat', 'Haruna', 'hafsatharun@gmail.com', '07012345676', '2', 'hafsatharun@gmail.com', '1', '1', '2018-08-12'),
(6, 'Maimuna', 'Jibrin', 'maimunaJ@gmail.com', '07012335676', '1', 'maimunaJ@gmail.com', '1', '1', '2018-08-12'),
(7, 'Fatima', 'Muhammad', 'faty@gmail.com', '08012335676', '2', 'faty@gmail.com', '1', '1', '2018-08-12'),
(8, 'Sulaiman', 'Idris', 'yescs4real@gmail.com', '07012345678', '2', 'yescs4real@gmail.com', '2', '1', '2018-08-13'),
(9, 'Abdullahi', 'Adamu', 'abdull@gmail.com', '07012345656', '2', 'abdull@gmail.com', '3', '1', '2018-08-13'),
(10, 'Mahmu', 'Adamu', 'abdullmah@gmail.com', '07012345656', '2', 'abdullmah@gmail.com', '2', '1', '2018-08-13'),
(11, 'Asmau', 'Wazeeri', 'wazeerasmau@gmail.com', '08167232343', '1', 'wazeerasmau@gmail.com', '2', '1', '2018-08-13'),
(12, 'Aminu', 'D. Usman', 'aminudusman@gmail.com', '07425776655', '2', 'aminudusman@gmail.com', '3', '1', '2018-08-16'),
(13, 'Hamit', 'Altintop', 'hamit.altintop@hotcake.net', '0050000599', '1', 'hamit.altintop@hotcake.net', '1', '1', '2018-08-16');

-- --------------------------------------------------------

--
-- Table structure for table `employee_category`
--

CREATE TABLE `employee_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_category`
--

INSERT INTO `employee_category` (`category_id`, `category_name`) VALUES
(1, 'Garbage Collector'),
(2, 'Sweeper'),
(3, 'Drainage Cleaner'),
(4, 'Inspector'),
(5, 'Supervisor'),
(6, 'Accountant'),
(7, 'Driver');

-- --------------------------------------------------------

--
-- Table structure for table `equipment_request`
--

CREATE TABLE `equipment_request` (
  `request_id` int(11) NOT NULL,
  `supervisor_id` varchar(100) NOT NULL,
  `equipment_id` varchar(20) NOT NULL,
  `lot_id` varchar(20) NOT NULL,
  `quantity_requested` int(10) NOT NULL,
  `comments` varchar(200) NOT NULL,
  `date_requested` varchar(20) NOT NULL,
  `request_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipment_request`
--

INSERT INTO `equipment_request` (`request_id`, `supervisor_id`, `equipment_id`, `lot_id`, `quantity_requested`, `comments`, `date_requested`, `request_status`) VALUES
(1, 'elnasabdallah@gmail.com', '1', '1', 45, 'We need this equipments ASAP', '2018-08-06', '1'),
(2, 'aminudusman@gmail.com', '1', '2', 10, 'soon', '2018-08-16', '0'),
(3, 'elnasabdallah@gmail.com', '2', '2', 20, 'We need this equipments ASAP', '2018-08-07', '1'),
(4, 'elnasabdallah@gmail.com', '3', '1', 10, 'We need by monday', '2018-08-07', '0'),
(5, 'aminudusman@gmail.com', '4', '1', 30, 'ASAP', '2018-08-24', '0'),
(6, 'elnasabdallah@gmail.com', '8', '1', 25, 'asap', '2018-09-10', '1');

-- --------------------------------------------------------

--
-- Table structure for table `equipment_table`
--

CREATE TABLE `equipment_table` (
  `equipment_id` int(11) NOT NULL,
  `equipment_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipment_table`
--

INSERT INTO `equipment_table` (`equipment_id`, `equipment_name`) VALUES
(1, 'Cutlass'),
(2, 'Broom'),
(3, 'Nose mask'),
(4, 'Hi Vis vest'),
(5, 'Site boats'),
(6, 'Bin bag(1 roll)'),
(7, 'Parker'),
(8, 'Shovel'),
(9, ' ');

-- --------------------------------------------------------

--
-- Table structure for table `lot_info`
--

CREATE TABLE `lot_info` (
  `lot_id` int(11) NOT NULL,
  `lot_name` varchar(100) NOT NULL,
  `lot_address` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lot_info`
--

INSERT INTO `lot_info` (`lot_id`, `lot_name`, `lot_address`) VALUES
(1, 'LOT A', 'Tudun wada shamaki ward'),
(2, 'LOT B', 'Tashan Dukku'),
(4, 'LOT C', 'Tashan Dukku (Fire Service)'),
(5, 'LOT DQ', 'Arawa quaters');

-- --------------------------------------------------------

--
-- Table structure for table `notification_tbl`
--

CREATE TABLE `notification_tbl` (
  `notification_id` int(11) NOT NULL,
  `from_id` varchar(20) NOT NULL,
  `to_id` varchar(20) NOT NULL,
  `message` varchar(200) NOT NULL,
  `date_received` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_tbl`
--

INSERT INTO `notification_tbl` (`notification_id`, `from_id`, `to_id`, `message`, `date_received`) VALUES
(1, 'dawaki@live.com', 'ALL SUPERVISORS', 'Everyone should assemble at the head office on 23rd August 2018 at 2PM', '2018-08-12'),
(2, 'dawaki@live.com', 'ALL SUPERVISORS', 'All supervisors should inform their members that they should come al9ng with their particulars to the head office on Monday 23rd August 2018, for verification. Thanks.', '2018-08-13'),
(3, 'dawaki@live.com', 'ALL SUPERVISORS', 'hello', '2018-09-10');

-- --------------------------------------------------------

--
-- Table structure for table `received_equipments`
--

CREATE TABLE `received_equipments` (
  `received_id` int(11) NOT NULL,
  `supervisor_id` varchar(100) NOT NULL,
  `equipment_id` varchar(20) NOT NULL,
  `lot_id` varchar(100) NOT NULL,
  `quantity_received` int(20) NOT NULL,
  `date_received` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `received_equipments`
--

INSERT INTO `received_equipments` (`received_id`, `supervisor_id`, `equipment_id`, `lot_id`, `quantity_received`, `date_received`) VALUES
(1, 'elnasabdallah@gmail.com', '2', '1', 3, '2018-08-13'),
(2, 'yescs4real@gmail.com', '2', '2', 20, '2018-08-14'),
(3, 'aminudusman@gmail.com', '6', '2', 5, '2018-08-16');

-- --------------------------------------------------------

--
-- Table structure for table `supervisors_tbl`
--

CREATE TABLE `supervisors_tbl` (
  `id` int(11) NOT NULL,
  `supervisor_id` varchar(100) NOT NULL,
  `lot_id` varchar(20) NOT NULL,
  `date_posted` varchar(20) NOT NULL,
  `posting_status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervisors_tbl`
--

INSERT INTO `supervisors_tbl` (`id`, `supervisor_id`, `lot_id`, `date_posted`, `posting_status`) VALUES
(1, 'elnasabdallah@gmail.com', '1', '2018-08-12', '1'),
(2, 'samailaadamumuazu@gmail.com', '2', '2018-08-12', '1'),
(3, 'yescs4real@gmail.com', '2', '2018-08-13', '0'),
(4, 'aminudusman@gmail.com', '2', '2018-08-16', '1'),
(5, 'faty@gmail.com', '4', '2018-09-10', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `pass_word` varchar(50) NOT NULL,
  `lot_id` varchar(100) NOT NULL,
  `access_level` varchar(4) NOT NULL,
  `account_status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`id`, `user_name`, `pass_word`, `lot_id`, `access_level`, `account_status`) VALUES
(1, 'dawaki@live.com', '1024', '1', 'AL1', '1'),
(2, 'elnasabdallah@gmail.com', '1024', '1', 'AL2', '1'),
(3, 'abmusadeeq@gmail.com', '1024', '1', 'AL1', '1'),
(4, 'samailaadamumuazu@gmail.com', '1024', '1', 'AL1', '1'),
(5, 'hafsatharun@gmail.com', '1024', '1', 'AL2', '1'),
(6, 'maimunaJ@gmail.com', '1024', '1', 'AL2', '1'),
(7, 'faty@gmail.com', '1024', '1', 'AL2', '1'),
(8, 'yescs4real@gmail.com', '1024', '2', 'AL2', '1'),
(9, 'abdull@gmail.com', '1368', '2', 'AL2', '1'),
(10, 'abdullmah@gmail.com', '9981', '2', 'AL2', '1'),
(11, 'wazeerasmau@gmail.com', '1294', '1', 'AL2', '1'),
(12, 'aminudusman@gmail.com', '1024', '2', 'AL2', '1'),
(13, 'hamit.altintop@hotcake.net', '4241', '1', 'AL2', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee_attendance`
--
ALTER TABLE `employee_attendance`
  ADD PRIMARY KEY (`attendance_id`),
  ADD UNIQUE KEY `duplicate_keys` (`employee_id`,`lot_id`,`attendance_date`);

--
-- Indexes for table `employee_biodata`
--
ALTER TABLE `employee_biodata`
  ADD PRIMARY KEY (`biodata_id`);

--
-- Indexes for table `employee_category`
--
ALTER TABLE `employee_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `equipment_request`
--
ALTER TABLE `equipment_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `equipment_table`
--
ALTER TABLE `equipment_table`
  ADD PRIMARY KEY (`equipment_id`);

--
-- Indexes for table `lot_info`
--
ALTER TABLE `lot_info`
  ADD PRIMARY KEY (`lot_id`);

--
-- Indexes for table `notification_tbl`
--
ALTER TABLE `notification_tbl`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `received_equipments`
--
ALTER TABLE `received_equipments`
  ADD PRIMARY KEY (`received_id`);

--
-- Indexes for table `supervisors_tbl`
--
ALTER TABLE `supervisors_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee_attendance`
--
ALTER TABLE `employee_attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `employee_biodata`
--
ALTER TABLE `employee_biodata`
  MODIFY `biodata_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `employee_category`
--
ALTER TABLE `employee_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `equipment_request`
--
ALTER TABLE `equipment_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `equipment_table`
--
ALTER TABLE `equipment_table`
  MODIFY `equipment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `lot_info`
--
ALTER TABLE `lot_info`
  MODIFY `lot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notification_tbl`
--
ALTER TABLE `notification_tbl`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `received_equipments`
--
ALTER TABLE `received_equipments`
  MODIFY `received_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `supervisors_tbl`
--
ALTER TABLE `supervisors_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

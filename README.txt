This is an android application meant to support company supervisors to:

    1- Take attendance of staff who are working at different sites, usually far from each other
    2- Manage equipments received or send to the sites.
    3- Send message to employees
    
    
### How to install the application ###
 
 1-  You must have a server to host your database.
 2-  I your server has phpMyadmin, then import the sql database.(the sql file is located in this repository at
       Supervisor-Assistant/databaseAndPhp/uiaorgng_supervisor_app.sql
 3-  The php scripts for processing the data on the server and the application apk (v1 and v2)are located at:
        Supervisor-Assistant/databaseAndPhp


### How to view the java and layout files ###

    For the Java files on this repository go to : Supervisor-Assistant/app/src/main/java/dawaki1/supervisor_assistant
    
    For the Layout files on this repository go to : Supervisor-Assistant/app/src/main/res
    
### How to replace your server details  ###

    go to Supervisor-Assistant/app/src/main/java/dawaki1/supervisor_assistant/ApiConstants.java

### Contact the developer for more information ###

email: dawakimuhammad@gmail.com
    

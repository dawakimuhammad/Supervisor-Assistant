package dawaki1.supervisor_assistant;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * With this class it contains functions that display the attendance statistics to the supervisor
 */
public class StatisticsActivity extends AppCompatActivity {

    SharedPreferences loginStatus;
    String lotId;
    TextView currentDateTxt,staffPresent,staffAbsent,percentPresent,percentAbsent,alertTextview;
    CardView noRecordsCard,presentCardView,absentCardView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        lotId = loginStatus.getString(ApiConstants.LOT_ID_PREF,"1");


        //find all the views using their ids specified in the alyout files
        staffAbsent = findViewById(R.id.numAbsentValues);
        percentAbsent = findViewById(R.id.percentAbsent);
        staffPresent = findViewById(R.id.numPresentValues);
        percentPresent = findViewById(R.id.percentagePresent);
        alertTextview = findViewById(R.id.noRecordAlert);

        noRecordsCard =  findViewById(R.id.noRecordCard);
        presentCardView = findViewById(R.id.cardViewStPresent);
        absentCardView = findViewById(R.id.cardViewStAbsent);


        //set today's date
        currentDateTxt = findViewById(R.id.statisticsDate);
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH)+1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String strCurrentDate = mDay + "-" + mMonth + "-" + mYear;
        currentDateTxt.setText(ApiConstants.stringToDateFormat(strCurrentDate,"d-M-yyyy","EEEE, dd MMM yyyy"));

        //get the statistics of employees
        getStatistics();

        final DatePickerDialog datePickerDialog = new DatePickerDialog(StatisticsActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strCurrentDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        currentDateTxt.setText(ApiConstants.stringToDateFormat(strCurrentDate,"d-M-yyyy","EEEE, dd MMMM yyyy"));
                        //show list based on selected date
                        getStatistics();
                    }
                }, mYear, mMonth, mDay);

        Button changeDate = findViewById(R.id.changeDateStat);
        changeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show();

            }
        });

    }

    /**
     * This function calculate the percentage of employees present or absent on a single lot
     */
    private  void  getStatistics(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_STATISTICS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.getString("reqStatus").equals("ok")) {

                        JSONObject rows = jsonObject.getJSONObject("result");

                        String present = rows.getString("totalPresent");
                        String absentees = rows.getString("totalAbsent");
                        String totalStaff = rows.getString("totalEmplo");

                        //calculate the staff present
                        staffPresent.setText(present+"/"+totalStaff);
                        percentPresent.setText("Percentage:"+String.valueOf((Integer.parseInt(present)/Integer.parseInt(totalStaff))*100)+"%");

                        //calculate the staff absent
                        staffAbsent.setText(absentees+"/"+totalStaff);
                        percentAbsent.setText("Percentage:"+String.valueOf((Integer.parseInt(absentees)/Integer.parseInt(totalStaff))*100)+"%");

                        noRecordsCard.setVisibility(View.INVISIBLE);
                        presentCardView.setVisibility(View.VISIBLE);
                        absentCardView.setVisibility(View.VISIBLE);
                    }
                    else{
                        alertTextview.setText("No Attendance Record Was Captured On "+currentDateTxt.getText().toString());
                        noRecordsCard.setVisibility(View.VISIBLE);
                        presentCardView.setVisibility(View.GONE);
                        absentCardView.setVisibility(View.GONE);

                    }


                } catch (JSONException e) {

                    Toast.makeText(getApplicationContext(), "No messages found", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();

                }

            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Unable to connect to the internet",Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("lot_id",lotId);
                params.put("stat_date",ApiConstants.stringToDateFormat(currentDateTxt.getText().toString(),"EEEE, dd MMM yyyy","yyyy-M-d"));
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent backButton = new Intent(getApplicationContext(),SelectPlotActivity.class);
                backButton.putExtra("nextActivity","statistics");
                startActivity(backButton);
                break;
        }
        return true;
    }


    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }


}

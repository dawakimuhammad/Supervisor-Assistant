package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddEmployeeActivity extends AppCompatActivity {

    EditText fnameEdtx,lnameEdtx,emailAddressEdtx,phoneNumberEdtx;
    Spinner categorySpner,lotIdSpner;
    String firstName,lastname,emailAddr,phoneNmbr,lotIdVal,categoryIdVal;

    static ArrayList<String> lotNameArray,lotIdArray,categoryIdArray,categoryNameArray = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_employee);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // find aech view from the layout
        fnameEdtx = findViewById(R.id.first_name);
        lnameEdtx = findViewById(R.id.last_name);
        emailAddressEdtx = findViewById(R.id.email);
        phoneNumberEdtx = findViewById(R.id.phone_number);
        categorySpner = findViewById(R.id.categoryChoosen);
        lotIdSpner = findViewById(R.id.chooseLotSpner);


        //fetch spinner options(LOT IDs) and store items in an arraylist
        fetchLotIdForSpinner();
        fetchCategoryIdForSpinner();

        //attach click listener to the select box
        lotIdSpner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                lotIdVal = AddEmployeeActivity.lotIdArray.get(position);
                //Toast.makeText(getApplicationContext(),AddEmployeeActivity.lotIdArray.get(position),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //check if user has selected something from the spinner
        categorySpner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                categoryIdVal = AddEmployeeActivity.categoryIdArray.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Button addEmployeeBtn = findViewById(R.id.createUser);
        addEmployeeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveEmployeeRecord();
            }
        });

    }

    /**
     * This method send the employee record to the server for storage
     */
    public  void saveEmployeeRecord(){

        firstName = fnameEdtx.getText().toString();
        lastname = lnameEdtx.getText().toString();
        emailAddr = emailAddressEdtx.getText().toString();
        phoneNmbr = phoneNumberEdtx.getText().toString();

        //check if all values are supplied to the database
        if(!firstName.equals("") && !lastname.equals("") && !emailAddr.equals("") && !phoneNmbr.equals("") ) {

            //send request to database
            StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.SAVE_EMPLOYEE,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            if (response.equals("ok")) {
                                Toast.makeText(getApplicationContext(), "Employee Added successfully", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getApplicationContext(), "Unable to add new employee,please try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Check your internet connectivity", Toast.LENGTH_SHORT).show();

                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    //specify parameters to post for storage into the database
                    Map<String, String> params = new HashMap<>();
                    params.put("first_name", firstName);
                    params.put("last_name", lastname);
                    params.put("phoneNumber", phoneNmbr);
                    params.put("lotIdSpner", lotIdVal);
                    params.put("emailAdd", emailAddr);
                    params.put("categorySpner", categoryIdVal);
                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jsonObjReq);

        }
        else{
            Toast.makeText(getApplicationContext(), "You must supply all the required details", Toast.LENGTH_SHORT).show();

        }

    }

    /**
     * The funtion below get all lots values from database
     * and then populate the Lot name dropdown
     */

    public  void fetchLotIdForSpinner(){

        AddEmployeeActivity.lotIdArray = new ArrayList<>();
        AddEmployeeActivity.lotNameArray = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_SPINNER_OPTIONS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.getString("reqStatus").equals("ok")) {

                        //get the JSON response and convert to array
                        JSONArray rows = jsonObject.getJSONArray("result");

                        //populate the lot spinner adapter arraylist
                        for(int i=0;i<rows.length();i++){

                            JSONObject singleRow = rows.getJSONObject(i);

                            //save the valu and name for display in the spinner view
                            AddEmployeeActivity.lotIdArray.add(singleRow.getString("lotId").toUpperCase());
                            AddEmployeeActivity.lotNameArray.add(singleRow.getString("lotName"));

                        }

                    }
                    else{
                        //store 0 to show spinner has no value
                        AddEmployeeActivity.lotIdArray.add("0");
                        AddEmployeeActivity.lotNameArray.add(jsonObject.getString("result"));

                    }


                } catch (JSONException e) {

                    Toast.makeText(getApplicationContext(), "No LOTS found, (spinner items)", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();

                }
                //populate the list view
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.custom_spinner_item,lotNameArray);
                adapter.setDropDownViewResource(R.layout.custom_spinner_item);
                lotIdSpner.setAdapter(adapter);

            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("fetch_id","lot_id");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    /**
     *This methode get category name from the database and populate the category select box
     */
    public  void fetchCategoryIdForSpinner(){

        //empty the arrays
        AddEmployeeActivity.categoryIdArray = new ArrayList<>();
        AddEmployeeActivity.categoryNameArray = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_SPINNER_OPTIONS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.getString("reqStatus").equals("ok")) {//check server response if its ok

                        JSONArray rows = jsonObject.getJSONArray("result");

                        //populate the lot spinner adapter arraylist
                        for(int i=0;i<rows.length();i++){

                            JSONObject singleRow = rows.getJSONObject(i);

                            //save the value and name for display in the dropdown view
                            AddEmployeeActivity.categoryIdArray.add(singleRow.getString("categoryId").toUpperCase());
                            AddEmployeeActivity.categoryNameArray.add(singleRow.getString("categoryName"));

                        }

                    }
                    else{
                        //store 0 to show spinner has no value
                        AddEmployeeActivity.categoryIdArray.add("0");
                        AddEmployeeActivity.categoryNameArray.add(jsonObject.getString("result"));

                    }


                } catch (JSONException e) {//no responce from the server

                    Toast.makeText(getApplicationContext(), "No Categories found, (spinner items)", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();

                }
                //populate the list view for the category name dropdown list
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.custom_spinner_item,categoryNameArray);

                adapter.setDropDownViewResource(R.layout.custom_spinner_item);
                categorySpner.setAdapter(adapter);

            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Unable to connect, switch on your internet",Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() {

                //define the parameters/values to post to the data
                Map<String,String> params = new HashMap<>();
                params.put("fetch_id","category_id");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        //if the user has logout,then always take them to login page
        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }


}

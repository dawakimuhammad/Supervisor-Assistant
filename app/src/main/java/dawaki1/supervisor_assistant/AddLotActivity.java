package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


/**
 * This lass is used by the super admin to add new lot name to the database
 */

public class AddLotActivity extends AppCompatActivity {

    String lotname,lotAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_lot);
        //add back button to action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final EditText lotNameEdtx = findViewById(R.id.lot_Name);
        final EditText lotAddressEdtx = findViewById(R.id.lot_address);


        Button addLot = findViewById(R.id.addLot);
        //listen to add lot button click
        addLot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the values entered from the textfield and save in Global variables
                lotname = lotNameEdtx.getText().toString();
                lotAddress = lotAddressEdtx.getText().toString();

                if(!lotname.equals("") && !lotAddress.equals("")){

                    //add new lot to database and notify user on success
                    addLot();

                    //set the form fields to empty for new entry
                    lotNameEdtx.setText("");
                    lotAddressEdtx.setText("");

                }
                else{
                    Toast.makeText(getApplicationContext(), "LOT name and address cannot be EMPTY", Toast.LENGTH_SHORT).show();

                }

            }
        });

    }

    /**
     * this method add new Lot name to the database
     * its for the super admin
     */

    public  void addLot(){

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ADD_LOT,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("LOTADD",response);
                        if(response.equals("ok")){
                            Toast.makeText(getApplicationContext(), "New Lot added successfully", Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Unable to add new lot,please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Check your internet connectivity",Toast.LENGTH_SHORT).show();

            }
        })
        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lot_name",lotname);
                params.put("lot_address",lotAddress);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);

    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        //if the user has logout,then always take them to login page

        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }
}

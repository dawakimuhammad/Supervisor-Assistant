package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This class contains functions that display options for the super-admin
 */
public class AdminMenuActivity extends AppCompatActivity {

    //define all the menu name in array
    String selectActionsArr [] = {"Assign Supervisor","Equipment Requests","Add New Employee","Add New Lot","Add New Category","Add New Equipment","Message All Supervisors","De-Assign Supervisors","Logout"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu);


        //populate the List view with actions to click on
        ListView manageEquipMenus = findViewById(R.id.superAdminActions);
        CustomAdapter customAdapter = new CustomAdapter();
        manageEquipMenus.setAdapter(customAdapter);


    }
    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return selectActionsArr.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            //get the custom ;ayout and populate the list view with options
            convertView = getLayoutInflater().inflate(R.layout.custom_list_actions,null);
            TextView txtAction =  convertView.findViewById(R.id.actionOptions);
            txtAction.setText(selectActionsArr[position]);
            CardView actionsCardview =  convertView.findViewById(R.id.actionCardView);

            //list to any list item clicked and
            // redirect the user to the activity
            actionsCardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent nextPage = null;
                    switch (position){
                        case 0:
                            nextPage = new Intent(AdminMenuActivity.this,AssignSupervisorActivity.class);
                            break;
                        case 1:
                            nextPage = new Intent(AdminMenuActivity.this,ApproveEquipmentRequest.class);
                            break;
                        case 2:
                            nextPage = new Intent(AdminMenuActivity.this,AddEmployeeActivity.class);
                            break;
                        case 3:
                            nextPage = new Intent(AdminMenuActivity.this,AddLotActivity.class);
                            break;
                        case 4:
                            nextPage = new Intent(AdminMenuActivity.this,AddCategory.class);
                            break;
                        case 5:
                            nextPage = new Intent(AdminMenuActivity.this,AddEquipmentActivity.class);
                            break;
                        case 6:
                            nextPage = new Intent(AdminMenuActivity.this,SendMessageActivity.class);
                            break;
                        case 7:
                            nextPage = new Intent(AdminMenuActivity.this,DeAssignSupervisor.class);
                            break;
                        case 8:
                            //logout button clicked
                            SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = loginStatus.edit();
                            editor.putString(ApiConstants.LOGIN_STATUS,"out");
                            editor.commit();
                            nextPage = new Intent(AdminMenuActivity.this,LoginActivity.class);
                            break;

                    }
                    //take user to page selected
                    startActivity(nextPage);
                }
            });


            return convertView;

        }
    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");
        //if the user has logout,then always take them to login page

        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }

}

package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class helps to display all the messages send by super admin to supervisors
 */
public class GeneralNotificationsActivity extends AppCompatActivity {


    static ArrayList<String> messagesArr,fromIdArray,dateMsgReceived = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_notifications);

        //set the back button icon.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //this function dsplays all messages once the screen is displayed
        fetchMessages();

    }

    class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return messagesArr.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //populate the list view
            convertView = getLayoutInflater().inflate(R.layout.custom_list_messages,null);
            TextView txtdateReceived = convertView.findViewById(R.id.dateReceived);
            TextView txtReceiver = convertView.findViewById(R.id.receiver);
            TextView txtMsgReceived = convertView.findViewById(R.id.msgReceived);

            txtdateReceived.append(dateMsgReceived.get(position));
            txtReceiver.append(fromIdArray.get(position));
            txtMsgReceived.setText(messagesArr.get(position));
            return convertView;
        }
    }

    /**
     * This function get all messages from the database for display
     */

    public  void fetchMessages(){

        //initiate arrayalist that would hold value of responce from the database
        GeneralNotificationsActivity.fromIdArray = new ArrayList<>();
        GeneralNotificationsActivity.messagesArr = new ArrayList<>();
        GeneralNotificationsActivity.dateMsgReceived = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_MESSAGES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.getString("reqStatus").equals("ok")) {

                        JSONArray rows = jsonObject.getJSONArray("result");

                        //populate the lot spinner adapter arraylist
                        for(int i=0;i<rows.length();i++){

                            JSONObject singleRow = rows.getJSONObject(i);
                            //save the value and name for display in the spinner view
                            GeneralNotificationsActivity.fromIdArray.add(singleRow.getString("fromName"));
                            GeneralNotificationsActivity.messagesArr.add(singleRow.getString("messages"));
                            GeneralNotificationsActivity.dateMsgReceived.add(singleRow.getString("dateReceived"));

                        }

                    }


                } catch (JSONException e) {

                    Toast.makeText(getApplicationContext(), "No messages found", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();

                }
                ListView messageListview =  findViewById(R.id.msgNotificationsList);
                CustomAdapter customAdapter = new CustomAdapter();
                messageListview.setAdapter(customAdapter);

            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Unable to connect to the internet",Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                //post message to the server
                Map<String,String> params = new HashMap<>();
                params.put("messages","get_messages");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }

}

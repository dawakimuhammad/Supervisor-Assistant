package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class AddCategory extends AppCompatActivity {

    String categoryName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get the layout and render it
        setContentView(R.layout.add_category);

        //add back button on action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //find the views from the xml layout
        final EditText categoryNameEdtx = findViewById(R.id.category_name);
        Button addCategory = findViewById(R.id.addCategory);


        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the values entered from the textfield and save in Global variables
                categoryName = categoryNameEdtx.getText().toString();

                if(!categoryName.equals("")) {

                    //add new category to database and notify the user
                    addCategory();

                    //Reset form for new entry
                    categoryNameEdtx.setText("");
                }else{
                    //tell  user to supply value to the input fields
                    Toast.makeText(getApplicationContext(), "Category name cannot be EMPTY", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    //This function add new work category to the database
    public  void addCategory(){

        //initiate request to the server side script to save data on database
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ADD_NEW_ITEM,
                new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) { //server has send back response

                        //check if reponse is the value expected
                        if(response.equals("ok")){

                            Toast.makeText(getApplicationContext(), "New category added successfully", Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Unable to add new category ,please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) { //server returns failure message

                Toast.makeText(getApplicationContext(),"Check your internet connectivity",Toast.LENGTH_SHORT).show();

            }
        })
        {

            @Override
            protected Map<String, String> getParams() {

                //define the parameters/values to post to the data
                Map<String, String> params = new HashMap<>();
                params.put("category_name",categoryName);
                params.put("item_id","category_id");

                return params;
            }

        };
        //ininiate http call to the server
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);

    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        //if the user has logout,then always take them to login page
        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }
}

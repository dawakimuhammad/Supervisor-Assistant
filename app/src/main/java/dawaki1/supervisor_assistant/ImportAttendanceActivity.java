package dawaki1.supervisor_assistant;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class ImportAttendanceActivity extends AppCompatActivity {
    SharedPreferences attendancePrefData,loginDetailsPref,lotIdOfflinePref;
    String  supervisorId;
    ProgressBar syncBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.synchronise);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        syncBar = findViewById(R.id.synProgressBar);

        attendancePrefData = getApplicationContext().getSharedPreferences(ApiConstants.ATTENDANCE_PREF_DATA, Context.MODE_PRIVATE);
        loginDetailsPref = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        lotIdOfflinePref = getApplicationContext().getSharedPreferences(ApiConstants.LOT_ID_OFFLINE_SAVE,Context.MODE_PRIVATE);
        supervisorId = loginDetailsPref.getString(ApiConstants.USERNAME_PREF,"dawaki@live.com");



        CardView synchroniseData = findViewById(R.id.cardViewSynchronise);

        synchroniseData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ApiConstants.isInternetAvailable(ImportAttendanceActivity.this)){

                    syncBar.setVisibility(View.VISIBLE);
                    Map<String, ?> allEntries1 = attendancePrefData.getAll();
                    String lot_id;
                    for (Map.Entry<String, ?> entry : allEntries1.entrySet()) {

                        //attendanceJSON =
                        lot_id = loginDetailsPref.getString(entry.getKey().toString(),"1");
                        synchroniseOfflineRecord(entry.getValue().toString(),entry.getKey(),lot_id);

                    }

                }
                else{
                    //show dialog and inform the user that internet is required
                    OfflineSaveDialog();
                }
            }
        });
    }

    public  void synchroniseOfflineRecord(final String attendanceRecordJson,final String dateCaptured,final String lotID){

        //check if internet is available, we then save to online server

            StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.SAVE_ATTENDANCE,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            syncBar.setVisibility(View.GONE);

                            if (response.equals("ok")) {


                                Toast.makeText(getApplicationContext(), "All offline records synchronised successfully", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getApplicationContext(), "Unable to synchronise record,please try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Check your internet connectivity", Toast.LENGTH_SHORT).show();

                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("lot_id", lotID);
                    params.put("superv_id", supervisorId);
                    params.put("atten_date",dateCaptured);
                    params.put("records", attendanceRecordJson);
                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jsonObjReq);

    }
    public Dialog OfflineSaveDialog() {

        final AlertDialog.Builder offlineSaveSuccess = new AlertDialog.Builder(ImportAttendanceActivity.this);

        offlineSaveSuccess.setMessage("The synchronisation requires internet, please SWITCH ON your internet and ensure that the connection is sustained uptill the time the backup finishes")
                // Add action buttons

                .setTitle("Offline Save")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });

        offlineSaveSuccess.create();
        return offlineSaveSuccess.show();


    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }


}

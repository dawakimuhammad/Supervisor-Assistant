package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * This class helps to send message to supervisors
 */
public class SendMessageActivity extends AppCompatActivity {

    SharedPreferences loginStatus;
    String usernamePref;
    EditText message2Send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_message);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        usernamePref = loginStatus.getString(ApiConstants.USERNAME_PREF,"daw@live.com");

        message2Send = findViewById(R.id.broadcastMsg);

        Button sendMessage= findViewById(R.id.sendMessage);

        //if the send button is clicked, then send the message
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageTyped = message2Send.getText().toString();

                if(!messageTyped.equals("")){//check if user has inputted values
                    sendMessage(messageTyped,usernamePref);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please you must enter a messgae to send", Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    /**
     * This send a message to the supervisors by passing the message aa parameter
     * @param broadcastMessage
     * @param senderUsername
     */

    private  void sendMessage(final String broadcastMessage,final String senderUsername){

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ADD_NEW_ITEM,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) { //decode responce from the server

                        if(response.equals("ok")){

                            Toast.makeText(getApplicationContext(), "Message send to all supervisors", Toast.LENGTH_SHORT).show();

                        }
                        else if (response.equals("error")){
                            Toast.makeText(getApplicationContext(), "Unable to send message ,please try again", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Please you must enter a messgae to send", Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Check your internet connectivity",Toast.LENGTH_SHORT).show();

            }
        })
        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("item_id","save_message");
                params.put("senderName",senderUsername);
                params.put("msgToSend",broadcastMessage);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);

    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }


}

package dawaki1.supervisor_assistant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Bundle;

/**
 * This class helps to display the welcome page of the application
 */

public class MainActivity extends Activity {

    SharedPreferences loginStatus;

    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //TestFairy.begin(this, "22598a699fff46a8bc1c7258a9eaffd7245e7917");

        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String userStatus = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        if(userStatus.equals("out")) { // if the user didnt logout

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent toLogin = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(toLogin);
                    finish();
                }
            }, SPLASH_TIME_OUT);

        }
        else{
            Intent nextPage = new Intent(MainActivity.this,MenuActivity.class);
            startActivity(nextPage);

        }

    }

}
package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The class helps the superadmin to assign supervisor role to empoyees
 */
public class AssignSupervisorActivity extends AppCompatActivity {

    static ArrayList<String> lotNameArray,lotIdArray = new ArrayList<>();
    Spinner lotIdSpner;
    String lotIdVal,supervUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.assign_supervisor);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lotIdSpner = findViewById(R.id.lotToAssignSpner);
        final EditText supervUsernameEdtx = findViewById(R.id.supervEmail);
        Button assignRoleBtn = findViewById(R.id.assignSupervisor);

        //get all lot and save to lt name dropdown
        fetchLotIdForSpinner();


        lotIdSpner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                lotIdVal = AssignSupervisorActivity.lotIdArray.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Assign button has been clicked by the user
        assignRoleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supervUsername =  supervUsernameEdtx.getText().toString();
                if(!supervUsername.equals("") && !lotIdVal.equals("")){
                    assignSupervisor(supervUsername,lotIdVal);
                }
                else{
                    Toast.makeText(getApplicationContext(), "All required fields must be completed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    /**
     * The funtion below get all lots values from database
     * and then populate the Lot name dropdown
     */
    public  void fetchLotIdForSpinner(){

        AssignSupervisorActivity.lotIdArray = new ArrayList<>();
        AssignSupervisorActivity.lotNameArray = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_SPINNER_OPTIONS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.getString("reqStatus").equals("ok")) {

                        JSONArray rows = jsonObject.getJSONArray("result");

                        //populate the lot spinner adapter arraylist
                        for(int i=0;i<rows.length();i++){

                            JSONObject singleRow = rows.getJSONObject(i);

                            //save the valu and name for display in the spinner view
                            AssignSupervisorActivity.lotIdArray.add(singleRow.getString("lotId").toUpperCase());
                            AssignSupervisorActivity.lotNameArray.add(singleRow.getString("lotName"));

                        }

                    }
                    else{
                        //store 0 to show spinner has no value
                        AddEmployeeActivity.lotIdArray.add("0");
                        AddEmployeeActivity.lotNameArray.add(jsonObject.getString("result"));

                    }


                } catch (JSONException e) {

                    Toast.makeText(getApplicationContext(), "No LOTS found, (spinner items)", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();

                }
                //populate the list view
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.custom_spinner_item,lotNameArray);

                adapter.setDropDownViewResource(R.layout.custom_spinner_item);

                lotIdSpner.setAdapter(adapter);

            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("fetch_id","lot_id");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    /**
     * This fuction assign new supervisor from the pool of supervisors
     * if the username of supervisor didnt exist, then it cannot be added
     * @param supervUsername
     * @param lotId
     */
    public void assignSupervisor(final String supervUsername,final String lotId){

            StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ASSIGN_SUPERVISOR,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            if (response.equals("ok")) { //correct responce received
                                Toast.makeText(getApplicationContext(), "New supervisor assigned to lot successfully", Toast.LENGTH_SHORT).show();
                            }
                            else if(response.equals("error1")) { //username didnt match
                                Toast.makeText(getApplicationContext(), "The username entered is not registered in the database", Toast.LENGTH_SHORT).show();
                            }
                            else{//internet connection not available
                                Toast.makeText(getApplicationContext(), "Unable to add new supervisor, please try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Check your internet connectivity", Toast.LENGTH_SHORT).show();

                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("supervUsername", supervUsername);
                    params.put("lot_posted", lotId);
                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jsonObjReq);


    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }

}
package dawaki1.supervisor_assistant;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MarkAttendanceActivity extends AppCompatActivity {

    static ArrayList<String>  empNameArr,empIdArr,jobCategoryArr,attStatusArr,empPhoneNumArr,emailAddressArr = new ArrayList<>();
    Map<String,String> attendanceMap = new HashMap<>();
    SharedPreferences loginStatus,attendancePref,lotIdOfflinePref;
    String lotId,supervisorId,usernamePrefs,passwordPrefs,markStatus="default";
    TextView currentDateTxt;
    boolean aboutToSend = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.mark_attendance);

        //create back button on the actionbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //get all session variables stored in shared preference
        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        lotId = loginStatus.getString(ApiConstants.LOT_ID_PREF,"1");
        supervisorId = loginStatus.getString(ApiConstants.USERNAME_PREF,"dawaki@live.com");
        usernamePrefs = loginStatus.getString(ApiConstants.USERNAME_PREF,"dawaki@live.com");
        passwordPrefs = loginStatus.getString(ApiConstants.PASSWORD_PREF,"1024");


        //set today's date
        currentDateTxt = findViewById(R.id.currentDate);
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH)+1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String strCurrentDate = mDay + "-" + mMonth + "-" + mYear;
        currentDateTxt.setText(ApiConstants.stringToDateFormat(strCurrentDate,"d-M-yyyy","EEEE, dd MMM yyyy"));

        //get employee resord from database for display
        fetchEmployeeRecord();

        final DatePickerDialog datePickerDialog = new DatePickerDialog(MarkAttendanceActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {

                        String strCurrentDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        currentDateTxt.setText(ApiConstants.stringToDateFormat(strCurrentDate,"d-M-yyyy","EEEE, dd MMMM yyyy"));
                        //show list based on selected date

                        markStatus="default";
                        fetchEmployeeRecord();

                    }
                }, mYear, mMonth, mDay);

        Button changeDate = findViewById(R.id.changeDate);
        changeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show();

            }
        });

        final Button markAllOrUnmarkAll = findViewById(R.id.markAllBtn);
        //handle the click on mark all button
        markAllOrUnmarkAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(markStatus.equals("markAll") || markStatus.equals("default")){ //default means first time click of button

                    //change the text on the button to unmark all
                    markAllOrUnmarkAll.setText("Mark All");

                    //checkboxes are to be marked while populating the list
                    markStatus = "markAll";

                    // call the function to mark all the checkboxes and set next action on button to UnMark All
                    mark_unmark_all("unmarkAll");


                }
                else if(markStatus.equals("unmarkAll")){

                    //change the text on the button to mark all since they are all unmarked
                    markAllOrUnmarkAll.setText("Unmark All");

                    //checkboxes are to be unmarked while populating the list
                    markStatus = "unmarkAll";

                    // call the function to mark all the checkboxes and set next action on button to Mark All
                    mark_unmark_all("markAll");

                }

            }
        });

        FloatingActionButton saveAttendanceBtn = findViewById(R.id.saveAttendanceBtn);
        saveAttendanceBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                //request user password fro authentication  before saving record
                attendancenceSecurityCheck(attendanceMap);

            }
        });

    }

    class CustomAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return empIdArr.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.custom_mark_attendance,null);
            final TextView txtEmpName = convertView.findViewById(R.id.markEmpName);
            final TextView txtEmpId = convertView.findViewById(R.id.markEmpId);
            final TextView txtContactMember = convertView.findViewById(R.id.contactMember);
            TextView txtEmpCategory = convertView.findViewById(R.id.markEmpCategory);
            CheckBox markChbx = convertView.findViewById(R.id.checkMark);


            //check if employees record were found
            if(empNameArr.size()>0) {


                String empId = empIdArr.get(position);
                String attStatusMark = attStatusArr.get(position);

                //populate the custom listview layout (custom_mark_attendance.xml)
                txtEmpName.setText(empNameArr.get(position));
                txtEmpId.append(empId);
                txtEmpCategory.append(jobCategoryArr.get(position));

                //store phone number and email into the textview for retrieval in contactMemberDialog()
                txtContactMember.setTag(R.string.emailTagId,emailAddressArr.get(position));
                txtContactMember.setTag(R.string.phoneNumTagId,empPhoneNumArr.get(position));


                //select the checkbox for those found to be present from previous record
                if(attStatusMark.equals("P")){
                    markChbx.setChecked(true);
                    //attendanceMap.put(empId,attStatusMark+empId);

                }

                //Get all employee's Id and mark them all as absent
                //'A' attached is for absent then store all their ids in an arrayList
                //Except if user is about to save attendance, then all entrie remain as they are
                if(!aboutToSend){

                    attendanceMap.put(empId, attStatusMark + empId);
                }

            }
            else {
                txtEmpName.setText("No employee record found");
            }


            //When user clicks on a checkbox to mark attendance, save the employee marked
            markChbx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                   @Override
                   public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                       String tempEmpId = txtEmpId.getText().toString().substring(6).trim();

                       if(isChecked) {
                           //save Id of employees present into ArrayList

                           attendanceMap.put(tempEmpId,"P"+tempEmpId);

                       }else {
                           //employee is absent save "A"
                           attendanceMap.put(tempEmpId,"A"+tempEmpId);

                       }

                   }
            });

            //when the user clicks on the 3  horizontal dots, then below function is called
            //to display more options dialog for call,sms and email

            txtContactMember.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //call the function that display the dialog for contacting memebers
                    contactMemberOptionsDialog(txtEmpName.getText().toString(),txtContactMember.getTag(R.string.emailTagId).toString(),txtContactMember.getTag(R.string.phoneNumTagId).toString());
                }
            });

            return convertView;
        }
    }

    private  void fetchEmployeeRecord(){

        //check if internet is available then fetch record
        if(ApiConstants.isInternetAvailable(this.getApplicationContext())) {

            // initialiase empty arrays and clean all record saved previously
            MarkAttendanceActivity.empNameArr = new ArrayList<>();
            MarkAttendanceActivity.empIdArr = new ArrayList<>();
            MarkAttendanceActivity.jobCategoryArr = new ArrayList<>();
            MarkAttendanceActivity.attStatusArr = new ArrayList<>();
            MarkAttendanceActivity.empPhoneNumArr = new ArrayList<>();
            MarkAttendanceActivity.emailAddressArr = new ArrayList<>();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_EMPLOYEES_URL, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        //store json object in shared preference for ofline usage
                        if (jsonObject.getString("reqStatus").equals("ok")) {

                            JSONArray rows = jsonObject.getJSONArray("result");

                            //Iterate through the JSON record to get individual values
                            for (int i = 0; i < rows.length(); i++) {

                                JSONObject singleRow = rows.getJSONObject(i);

                                //save the employee records returned in JSON format from the database into array list
                                MarkAttendanceActivity.empNameArr.add(singleRow.getString("lName").toUpperCase() + ", " + singleRow.getString("fName"));
                                MarkAttendanceActivity.emailAddressArr.add(singleRow.getString("email"));
                                MarkAttendanceActivity.empPhoneNumArr.add(singleRow.getString("phone"));
                                MarkAttendanceActivity.jobCategoryArr.add(singleRow.getString("category"));
                                MarkAttendanceActivity.empIdArr.add(String.format("%03d", Integer.parseInt(singleRow.getString("empId"))));

                                if(markStatus.equals("markAll")){

                                    //mark checkboxes present. P in argument stands  for PRESENT
                                    MarkAttendanceActivity.attStatusArr.add("P");

                                }
                                else if(markStatus.equals("unmarkAll")){
                                    //mark check boxes absent. A in argument statd for ABSENT
                                    MarkAttendanceActivity.attStatusArr.add("A");
                                }
                                else {
                                    //mark all button was not cliked, so show default view
                                    MarkAttendanceActivity.attStatusArr.add(singleRow.getString("attd_status"));

                                }

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("result"), Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //populate the list view
                    ListView plotOptions = findViewById(R.id.employeeListview);
                    CustomAdapter customAdapter = new CustomAdapter();
                    plotOptions.setAdapter(customAdapter);


                }
            },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(getApplicationContext(), "Internet  is unavailable,please connect your device", Toast.LENGTH_SHORT).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("lot_id", lotId);
                    params.put("atten_date", ApiConstants.stringToDateFormat(currentDateTxt.getText().toString(), "EEEE, dd MMM yyyy", "yyyy-M-d"));
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else{
            OfflineSaveDialog("Employee list cannot be fetched due to UNAVAILABLE DATA CONNECTION, please connect your device to internet and try again","Internet Unavailable");
            Toast.makeText(getApplicationContext(), "Internet  is unavailable,please connect your device to SEE THE LIST", Toast.LENGTH_SHORT).show();
        }
    }

    private  void saveAttendanceRecord(final String attendanceRecordJsons){
        //check if internet is available, we then save to online server
        if(ApiConstants.isInternetAvailable(this.getApplicationContext())) {

            StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.SAVE_ATTENDANCE,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            if (response.equals("ok")) {
                                Toast.makeText(getApplicationContext(), "Attendance  SAVED/UPDATED successfully", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getApplicationContext(), "Unable to save attendance, try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_SHORT).show();

                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("lot_id", lotId);
                    params.put("superv_id", supervisorId);
                    params.put("atten_date", ApiConstants.stringToDateFormat(currentDateTxt.getText().toString(), "EEEE, dd MMM yyyy", "yyyy-M-d"));
                    params.put("records", attendanceRecordJsons);
                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jsonObjReq);
        }
        else{//else we backup the attendance record in a sharedprefence

            attendancePref = getSharedPreferences(ApiConstants.ATTENDANCE_PREF_DATA,Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = attendancePref.edit();
            //save each attendance record with using the attendance date as key
            String keyDate = ApiConstants.stringToDateFormat(currentDateTxt.getText().toString(), "EEEE, dd MMM yyyy", "yyyy-M-d");
            editor.putString(keyDate,attendanceRecordJsons);
            editor.commit();

            //save lot id also

            lotIdOfflinePref = getSharedPreferences(ApiConstants.LOT_ID_OFFLINE_SAVE,Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2 = lotIdOfflinePref.edit();
            editor2.putString(keyDate,lotId);
            editor2.commit();

            //save each attendance record with using the attendance date as key

            OfflineSaveDialog("Due to unavailability of INTERNET, your attendance record was saved on the phone and would be synchronised with the remote database when the internet becomes available","Offline Save");

        }
    }

    private Dialog attendancenceSecurityCheck(final Map<String, String> attendanceJson1) {

        aboutToSend = true;
        final LayoutInflater inflater = this.getLayoutInflater();
        final AlertDialog.Builder confirmDialogue = new AlertDialog.Builder(MarkAttendanceActivity.this);
        final View customDialogueView = inflater.inflate(R.layout.confirm_password_dialog,null);
        confirmDialogue.setView(customDialogueView)
                // Add action buttons

                .setTitle("Security Check")
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        //check password here
                        EditText passwordEdtx = customDialogueView.findViewById(R.id.customPassDialogue);
                        checkUser(usernamePrefs,passwordEdtx.getText().toString(),attendanceJson1);
                        //String userEnteredPassword = passwordEdtx.getText().toString();

                    }
                })
                .setNegativeButton("Cancel",  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        confirmDialogue.create();
        return confirmDialogue.show();


    }

    private Dialog OfflineSaveDialog(String message,String tittle) {

        final AlertDialog.Builder offlineSaveSuccess = new AlertDialog.Builder(MarkAttendanceActivity.this);

        offlineSaveSuccess.setMessage(message)
                // Add action buttons

                .setTitle(tittle)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });

        offlineSaveSuccess.create();
        return offlineSaveSuccess.show();


    }

    private Dialog contactMemberOptionsDialog (String employeeName,final String emailAddress, final String phoneNumber) {


        final LayoutInflater inflater = this.getLayoutInflater();
        final AlertDialog.Builder contactDialogue = new AlertDialog.Builder(MarkAttendanceActivity.this);
        final View customDialogueView = inflater.inflate(R.layout.custom_contact_member_dialog,null);

        CardView smsCardItem = customDialogueView.findViewById(R.id.smsCardView);
        CardView phoneCallCardItem = customDialogueView.findViewById(R.id.phoneCallCardView);
        CardView emailCardItem = customDialogueView.findViewById(R.id.emailCardView);


        smsCardItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openSMSApp = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+phoneNumber));
                startActivity(openSMSApp);
                Toast.makeText(getApplicationContext(),"SMS SEnd",Toast.LENGTH_LONG);
            }
        });
        phoneCallCardItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openCallDialer = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:"+phoneNumber));
                startActivity(openCallDialer);
                Toast.makeText(getApplicationContext(),"hone SEnd",Toast.LENGTH_LONG);

            }
        });
        emailCardItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openEmailApp = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",emailAddress, null));

                if (openEmailApp.resolveActivity(getPackageManager()) == null) {

                    Toast.makeText(getBaseContext(), "Configure your email client to continue", Toast.LENGTH_SHORT).show();

                } else {
                    startActivity(Intent.createChooser(openEmailApp, "Please choose your email app"));

                }


            }
        });

        contactDialogue.setView(customDialogueView)
                // Add action buttons

                .setTitle(employeeName)
                .setNegativeButton("Cancel",  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        contactDialogue.create();
        return contactDialogue.show();


    }

    private void checkUser (final String username,final String password,final Map<String, String> attendanceJson){

        if(ApiConstants.isInternetAvailable(this.getApplicationContext())) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.LOGIN_URL,
                    new Response.Listener<String>() {


                        @Override
                        public void onResponse(String response) {


                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                JSONObject attRecords = new JSONObject(attendanceJson);


                                if (jsonObject.getString("reqStatus").equals("ok")) {
                                    saveAttendanceRecord(attRecords.toString());

                                } else {
                                    //if duplicate entry is found, it is also same problem
                                    attendancenceSecurityCheck(attendanceJson);
                                    Toast.makeText(getApplicationContext(), jsonObject.getString("result"), Toast.LENGTH_SHORT).show();

                                }

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(getApplicationContext(), "Connect your device to internet", Toast.LENGTH_SHORT).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("username", username);
                    params.put("password", password);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }
        else{
            if(passwordPrefs.equals(password)){
                JSONObject attRecords = new JSONObject(attendanceJson);
                saveAttendanceRecord(attRecords.toString());
            }
            else{
                attendancenceSecurityCheck(attendanceJson);
                Toast.makeText(getApplicationContext(),"Invalid password supplied", Toast.LENGTH_SHORT).show();

            }

        }
    }

    private void mark_unmark_all(String markStatusVal){

        //fetch record by performing un(mark all)
        fetchEmployeeRecord();

        //set the status of the next action on the button mark/unmark
        markStatus = markStatusVal;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent backButton = new Intent(getApplicationContext(),SelectPlotActivity.class);
                backButton.putExtra("nextActivity","attendance");
                startActivity(backButton);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }


}

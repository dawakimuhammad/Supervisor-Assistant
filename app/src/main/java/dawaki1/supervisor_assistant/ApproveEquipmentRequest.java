package dawaki1.supervisor_assistant;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is used by super admin to approve request of equipment send to them
 */
public class ApproveEquipmentRequest extends AppCompatActivity {

    private static ArrayList<String>  equipmentNamesArr,equipmentQuantArr,equipRequestDate,equipmentReqId,equipmentLotId = new ArrayList<String>();
    private String lotId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_equipment_request);

        //add back button on action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        lotId = loginStatus.getString(ApiConstants.LOT_ID_PREF,"1");

        //fetch all request sent to to super admin for display
        fetchAllRequests();

    }

    /**
     * this method get all tools request from the database and display to super admin
     * the request are diplayed in list format according to dates
     */
    private  void fetchAllRequests(){

        //check if internet is available
        if(ApiConstants.isInternetAvailable(ApproveEquipmentRequest.this)) {

            // initiates all arraylist that would hold the fetched records
            ApproveEquipmentRequest.equipmentNamesArr = new ArrayList<>();
            ApproveEquipmentRequest.equipmentQuantArr = new ArrayList<>();
            ApproveEquipmentRequest.equipRequestDate = new ArrayList<>();
            ApproveEquipmentRequest.equipmentReqId = new ArrayList<>();
            ApproveEquipmentRequest.equipmentLotId = new ArrayList<>();

            //send request to database
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_ALL_REQUESTS, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        //decode JSON strig received from database
                        if (jsonObject.getString("reqStatus").equals("ok")) {

                            JSONArray rows = jsonObject.getJSONArray("result");

                            for (int i = 0; i < rows.length(); i++) {

                                JSONObject singleRow = rows.getJSONObject(i);

                                //save all the values in array
                                ApproveEquipmentRequest.equipmentNamesArr.add(singleRow.getString("eqiupName"));
                                ApproveEquipmentRequest.equipmentQuantArr.add(singleRow.getString("quantityReq"));
                                ApproveEquipmentRequest.equipRequestDate.add(singleRow.getString("dateReq"));
                                ApproveEquipmentRequest.equipmentReqId.add(singleRow.getString("reqId"));
                                ApproveEquipmentRequest.equipmentLotId.add(singleRow.getString("lotName"));


                            }
                        } else {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("result"), Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // set the results into custom list view
                    ListView plotOptions = findViewById(R.id.adminEquipRequestList);
                    ApproveEquipmentRequest.CustomAdapter customAdapter = new ApproveEquipmentRequest.CustomAdapter();
                    plotOptions.setAdapter(customAdapter);


                }
            },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(getApplicationContext(), "Internet connection not available, please connect and try again", Toast.LENGTH_SHORT).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("lotID", lotId);
                    params.put("accessLevel", "admin");
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else{
            //internet not available,
            //prompt the user with dialog box
            final AlertDialog.Builder alertBuilder= new AlertDialog.Builder(ApproveEquipmentRequest.this);

            alertBuilder.setMessage("The list of EQUIPMENT REQUESTS cannot be displayed due to lack of INTERNET connection, please connect your device and try again")
                    // Add action buttons

                    .setTitle("Internet Connection")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                        }
                    });

            alertBuilder.create();
            alertBuilder.show();

        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return equipmentNamesArr.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            //get the custom list view to populate
            convertView = getLayoutInflater().inflate(R.layout.custom_admin_request_list,null);

            //get the textview from the layout files
            final TextView txtEquipName =  convertView.findViewById(R.id.adminTxtEquipName);
            TextView txtQuantity =  convertView.findViewById(R.id.adminTxtQuantity);
            TextView txtRequestDate =  convertView.findViewById(R.id.adminTxtRequestDate);
            TextView txtLotName =  convertView.findViewById(R.id.adminTxtLotName);


            Button approveBtn = convertView.findViewById(R.id.btnApproveRequest);

            //populate the list with equipment request information
            txtEquipName.setText(String.valueOf(position+1)+" - "+equipmentNamesArr.get(position).toUpperCase());
            txtQuantity.setText("Quantity Requested: "+equipmentQuantArr.get(position));
            txtRequestDate.setText(ApiConstants.stringToDateFormat(equipRequestDate.get(position),"yyyy-M-d","dd-MMM-yyyy"));
            txtLotName.setText("Lot Name: "+equipmentLotId.get(position));
            //save teh request id in the equipment name TAG
            txtEquipName.setTag(equipmentReqId.get(position));

            //when the super Admin click on approve request, then perform the function below
            approveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //show dialog box to confirm de-approval
                    final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ApproveEquipmentRequest.this);

                    alertBuilder.setMessage("Do you really want to approve this request?")

                            .setTitle("Confirmation")
                            //if yes is selected, then do the action
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    //approve request by passing the request Id set from line 186
                                    ApproveRequest(txtEquipName.getTag().toString());
                                    dialog.cancel();

                                }
                            })
                            //if no is selected, then do nothing

                            .setNegativeButton("Cancel",  new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    alertBuilder.create();
                    alertBuilder.show();

                }
            });

            return convertView;

        }
    }

    /**
     * This method is called if the user clicks approve request button
     * @param requestId
     */
    private void ApproveRequest(final String requestId){


        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ADD_NEW_ITEM,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        if(response.equals("ok")){

                            //refresh the screen and show success message
                            fetchAllRequests();
                            Toast.makeText(getApplicationContext(), "Equipment Request Approved successfully", Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Unable to approve request ,please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Check your internet connectivity",Toast.LENGTH_SHORT).show();

            }
        })
        {

            @Override
            protected Map<String, String> getParams() {

                //send the post request parameters
                Map<String, String> params = new HashMap<>();
                params.put("reqID",requestId);
                params.put("item_id","approve_request");

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);


    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        //if the user has logout,then always take them to login page
        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }

}

package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This class is for manage equipment menu, which display the list of actions the supervisor
 * can do to manage equipments in the company
 */
public class ManageEquipmentsActivity extends AppCompatActivity {

    //store the list of actions in an array
    String selectActionsArr [] = {"Received Equipments","Request Equipments","Equipment Request Status"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_equipments);

        //set the back button on top
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //call the fucntion that populates the sub menu of the super admin
        ListView manageEquipMenus = findViewById(R.id.manageEquipActions);
        CustomAdapter customAdapter = new CustomAdapter();
        manageEquipMenus.setAdapter(customAdapter);
    }


    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return selectActionsArr.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.custom_list_actions,null);
            TextView txtAction =  convertView.findViewById(R.id.actionOptions);
            txtAction.setText(selectActionsArr[position]);
            CardView actionsCardview =  convertView.findViewById(R.id.actionCardView);

            //listen to any option selectd by supervisor and then take them appropriate activity
            actionsCardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent nextPage = null;
                    switch (position){
                        case 0:
                            nextPage = new Intent(ManageEquipmentsActivity.this,SuppliedEquipmentsActivity.class);
                            break;
                        case 1:
                            nextPage = new Intent(ManageEquipmentsActivity.this,RequestEquipmentsActivity.class);
                            break;
                        case 2:
                            nextPage = new Intent(ManageEquipmentsActivity.this,EquipmentRequestListActivity.class);
                            break;

                    }
                    startActivity(nextPage);
                }
            });


            return convertView;

        }
    }
    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }

}

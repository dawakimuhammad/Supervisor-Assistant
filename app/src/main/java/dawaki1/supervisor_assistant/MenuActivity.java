package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;


public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    CardView takeAttendance,manageTools,notifications,statisticsMenu,backupmenu,logout;
    Intent intent;
    SharedPreferences loginStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        if(login_status.equals("out")){

            intent = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(intent);

        }


        takeAttendance =  findViewById(R.id.takeAttendanceCard);
        manageTools = findViewById(R.id.manageToolCard);
        notifications =  findViewById(R.id.messageCard);
        statisticsMenu =  findViewById(R.id.statisticsCard);
        backupmenu =  findViewById(R.id.backupCard);
        logout =  findViewById(R.id.logoutCard);


        takeAttendance.setOnClickListener(this);
        manageTools.setOnClickListener(this);
        notifications.setOnClickListener(this);
        statisticsMenu.setOnClickListener(this);
        backupmenu.setOnClickListener(this);
        logout.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.takeAttendanceCard:
                intent = new Intent(getApplicationContext(),SelectPlotActivity.class);
                intent.putExtra("nextActivity","attendance");
                break;
            case R.id.manageToolCard:

                intent = new Intent(getApplicationContext(),ManageEquipmentsActivity.class);

                break;
            case R.id.messageCard:
                intent = new Intent(getApplicationContext(),GeneralNotificationsActivity.class);

                break;
            case R.id.statisticsCard:
                intent = new Intent(getApplicationContext(),SelectPlotActivity.class);
                intent.putExtra("nextActivity","statistics");

                break;
            case R.id.backupCard:
                intent = new Intent(getApplicationContext(),ImportAttendanceActivity.class);

                break;
            case R.id.logoutCard:

                //clear all login session data to prevent access to app
                SharedPreferences.Editor editor = loginStatus.edit();
                editor.putString(ApiConstants.LOGIN_STATUS,"out");
                editor.commit();

                intent = new Intent(getApplicationContext(),LoginActivity.class);

                break;
        }

        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }
}

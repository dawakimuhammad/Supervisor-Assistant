package dawaki1.supervisor_assistant;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EquipmentRequestListActivity extends AppCompatActivity {

    private static ArrayList<String>  equipmentNamesArr,equipmentQuantArr,equipRequestDate,equipmentStatus = new ArrayList<String>();
    private String lotId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.equipment_request_list);

        //add the back button to te action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        lotId = loginStatus.getString(ApiConstants.LOT_ID_PREF,"1");

        //call the fuction to display all the requests received
        fetchAllRequests();

    }

    /**
     * This method get all the equipment requested by supervisors and display them
     */
    private  void fetchAllRequests(){

        if(ApiConstants.isInternetAvailable(EquipmentRequestListActivity.this)) {


            EquipmentRequestListActivity.equipmentNamesArr = new ArrayList<>();
            EquipmentRequestListActivity.equipmentQuantArr = new ArrayList<>();
            EquipmentRequestListActivity.equipRequestDate = new ArrayList<>();
            EquipmentRequestListActivity.equipmentStatus = new ArrayList<>();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_ALL_REQUESTS, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("reqStatus").equals("ok")) {

                            JSONArray rows = jsonObject.getJSONArray("result");

                            for (int i = 0; i < rows.length(); i++) {

                                JSONObject singleRow = rows.getJSONObject(i);
                                //decode JSON and store value in arralist
                                EquipmentRequestListActivity.equipmentNamesArr.add(singleRow.getString("eqiupName"));
                                EquipmentRequestListActivity.equipmentQuantArr.add(singleRow.getString("quantityReq"));
                                EquipmentRequestListActivity.equipRequestDate.add(singleRow.getString("dateReq"));
                                EquipmentRequestListActivity.equipmentStatus.add(singleRow.getString("reqStatus"));

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("result"), Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ListView plotOptions = findViewById(R.id.equipRequestList);
                    EquipmentRequestListActivity.CustomAdapter customAdapter = new EquipmentRequestListActivity.CustomAdapter();
                    plotOptions.setAdapter(customAdapter);


                }
            },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(getApplicationContext(), "Internet connection not available, please connect and try again", Toast.LENGTH_SHORT).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lotID", lotId);
                    params.put("accessLevel", "notAdmin");
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else{ //internet not available

            final AlertDialog.Builder alertBuilder= new AlertDialog.Builder(EquipmentRequestListActivity.this);

            alertBuilder.setMessage("The list of EQUIPMENT REQUESTS cannot be displayed due to lack of INTERNET connection, please connect your device and try again")
                    // Add action buttons

                    .setTitle("Internet Connection")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                        }
                    });

            alertBuilder.create();
            alertBuilder.show();

        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return equipmentNamesArr.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.custom_request_list,null);

            //get the text
            TextView txtEquipName =  convertView.findViewById(R.id.txtEquipName);
            TextView txtQuantity =  convertView.findViewById(R.id.txtQuantity);
            TextView txtRequestDate =  convertView.findViewById(R.id.txtRequestDate);
            TextView txtRequestStatus =  convertView.findViewById(R.id.txtRequestStatus);


            //populate the list with equipment request information
            txtEquipName.setText(String.valueOf(position+1)+" - "+equipmentNamesArr.get(position).toUpperCase());
            txtQuantity.setText("Quantity Requested: "+equipmentQuantArr.get(position));
            txtRequestDate.setText(ApiConstants.stringToDateFormat(equipRequestDate.get(position),"yyyy-M-d","dd-MMM-yyyy"));

            //if the super admin has approved the request
            if(equipmentStatus.get(position).equals("1")){
                txtRequestStatus.setText("Request Status: Approved");
                txtRequestStatus.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            }
            //if the super admin did not approved the request
            //show pending red color
            else{
                txtRequestStatus.setText("Request Status: Pending");
                txtRequestStatus.setTextColor(getResources().getColor(R.color.danger));

            }
            return convertView;

        }
    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }

}

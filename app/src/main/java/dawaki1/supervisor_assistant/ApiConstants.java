package dawaki1.supervisor_assistant;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by dawakz on 20/07/2018.
 */

//http://192.168.64.2/supervisor/

public class ApiConstants {
    public static final String BASE_URL = "http://uia.org.ng/androidApp/";
    public static final String LOGIN_URL = BASE_URL+"login.php";
    public static final String GET_EMPLOYEES_URL = BASE_URL+"getEmployees.php";
    public static final String SAVE_ATTENDANCE = BASE_URL+"saveAttendance.php";
    public static final String SELECT_LOT = BASE_URL+"selectLot.php";
    public static final String SAVE_EMPLOYEE = BASE_URL+"saveEmployee.php";
    public static final String ADD_LOT = BASE_URL+"addLot.php";
    public static final String ADD_NEW_ITEM = BASE_URL+"addNewItem.php";
    public static final String GET_SPINNER_OPTIONS = BASE_URL+"getSpinnerOptions.php";
    public static final String ASSIGN_SUPERVISOR = BASE_URL+"assignSupervisor.php";
    public static final String GET_MESSAGES = BASE_URL+"getMessages.php";
    public static final String GET_STATISTICS = BASE_URL+"getStatistics.php";
    public static final String GET_ALL_SUPERVISORS = BASE_URL+"getAllSupervisors.php";
    public static final String GET_ALL_REQUESTS = BASE_URL+"getAllRequests.php";




    public static final String LOGIN_PREF_DATA = "sharedPrefData";
    public static final String ATTENDANCE_PREF_DATA = "attendancePref";
    public static final String LOT_ID_PREF = "lotIdPref";
    public static final String SUPERV_ID_PREF = "supervIdPref";
    public static final String USERNAME_PREF = "usernamePref";
    public static final String LOT_ID_OFFLINE_SAVE = "lotIdOfflineSave";
    public static final String PASSWORD_PREF = "passwordPref";
    public static final String LOGIN_STATUS = "login_status" ;


    public static  String stringToDateFormat(String dateString,String inputFormat,String outputFormat ) {

        Date sdfIn=null;
        try {
            sdfIn = new SimpleDateFormat(inputFormat, Locale.ENGLISH).parse(dateString);
        } catch (ParseException e1) {

            e1.printStackTrace();
        }

        SimpleDateFormat sdfOut = new SimpleDateFormat(outputFormat, Locale.ENGLISH);

        String newStringDate = sdfOut.format(sdfIn);

        return newStringDate;
    }

//    public static boolean isInternetAvailable() {
//        try {
//            final InetAddress address = InetAddress.getByName("www.google.com");
//            if (address.equals("") || address.equals(null)  ) {
//
//
//                return false;
//            } else {
//                return true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }

    public static boolean isInternetAvailable(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}

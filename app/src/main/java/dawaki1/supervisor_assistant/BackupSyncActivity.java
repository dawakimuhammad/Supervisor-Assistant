package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This class displays the menu for the supevisor to perform backup
 */
public class BackupSyncActivity extends AppCompatActivity {

    String selectActionsArr [] = {"Import And Synchronise Attendance","Export Attendance","Supply/Request History "};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.backup_sync);

        ListView listView = findViewById(R.id.backupActions);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);

    }


    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return selectActionsArr.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //populate the list items
            convertView = getLayoutInflater().inflate(R.layout.custom_list_actions,null);
            TextView txtAction = (TextView) convertView.findViewById(R.id.actionOptions);
            txtAction.setText(selectActionsArr[position]);
            return convertView;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }
}

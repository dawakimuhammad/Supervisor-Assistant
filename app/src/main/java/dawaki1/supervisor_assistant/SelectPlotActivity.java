package dawaki1.supervisor_assistant;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SelectPlotActivity extends AppCompatActivity {

    String lotEmployessArr [] = {"8","10","35"};
    String nextActivity="";


    static ArrayList<String>  lotIdArr,lotAddressArr,lotNames = new ArrayList<String>();

    SharedPreferences loginStatus;
    String lotId,supervisorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_plot);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle extras = getIntent().getExtras();
        //get the activity from which the user comes from, either mark attendance or statistics page
        if (extras != null) {
            nextActivity = extras.getString("nextActivity");
        }else {
            nextActivity = "extra not set";
        }



        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        lotId = loginStatus.getString(ApiConstants.LOT_ID_PREF,"1");
        supervisorId = loginStatus.getString(ApiConstants.USERNAME_PREF,"dawaki@live.com");

        fetchSupervisorLots();
        
    }

    class CustomAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return lotIdArr.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.custom_list_select_plot,null);
            final TextView txtLotId =  convertView.findViewById(R.id.lotName);
            TextView txtLotAddress =  convertView.findViewById(R.id.lotAddresss);
            TextView txtLotEmployees =  convertView.findViewById(R.id.lotEmployees);
            CardView lotOptionsCard =  convertView.findViewById(R.id.selectLotCard);

            lotOptionsCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String tempLotId = txtLotId.getText().toString().substring(19,txtLotId.length()-1).trim();

                    SharedPreferences.Editor editor = loginStatus.edit();
                    editor.putString(ApiConstants.LOT_ID_PREF,tempLotId);
                    editor.commit();

                    Intent toNextActivity;

                    if(nextActivity.equals("attendance")){
                        toNextActivity = new Intent(getApplicationContext(),MarkAttendanceActivity.class);

                    }
                    else{
                        toNextActivity = new Intent(getApplicationContext(),StatisticsActivity.class);
                    }

                    startActivity(toNextActivity);

                }
            });



            if(lotIdArr.size()>0) {

                txtLotId.append(lotNames.get(position).toUpperCase()+" (LOT-"+lotIdArr.get(position)+")");
                txtLotAddress.setText(lotAddressArr.get(position));
                txtLotEmployees.setText(lotEmployessArr[position] + txtLotEmployees.getText());
            }

            else {
                txtLotAddress.setText("No employee record found");
            }


            return convertView;
        }
    }

    /**
     * the method fetch all lot information and display them in list format
     */
    private  void fetchSupervisorLots(){

        if(ApiConstants.isInternetAvailable(SelectPlotActivity.this)) {


            SelectPlotActivity.lotIdArr = new ArrayList<>();
            SelectPlotActivity.lotAddressArr = new ArrayList<>();
            SelectPlotActivity.lotNames = new ArrayList<>();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.SELECT_LOT, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {


                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("reqStatus").equals("ok")) {

                            //decode the json into an array
                            JSONArray rows = jsonObject.getJSONArray("result");

                            for (int i = 0; i < rows.length(); i++) {

                                JSONObject singleRow = rows.getJSONObject(i);
                                SelectPlotActivity.lotIdArr.add(singleRow.getString("lotIdDB"));
                                SelectPlotActivity.lotNames.add(singleRow.getString("lotName"));
                                SelectPlotActivity.lotAddressArr.add(singleRow.getString("lotAddress"));

                                //calculate number of employess using php and return to update
                                //SelectPlotActivity.empIdArr.add(String.format("%03d", Integer.parseInt(singleRow.getString("empId"))));

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("result"), Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ListView plotOptions = findViewById(R.id.plotOptions);
                    CustomAdapter customAdapter = new CustomAdapter();
                    plotOptions.setAdapter(customAdapter);


                }
            },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(getApplicationContext(), "Unavailable internet connection, please connect and try again", Toast.LENGTH_SHORT).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lot_id", lotId);
                    params.put("superv_id", supervisorId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else{

            //show error to user because lot info cannot be found
            final AlertDialog.Builder offlineSaveSuccess = new AlertDialog.Builder(SelectPlotActivity.this);

            offlineSaveSuccess.setMessage("The LOTS cannot be displayed due to lack of INTERNET connection, please connect your device and try again")
                    // Add action buttons

                    .setTitle("Unavailable Internet")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                        }
                    });

            offlineSaveSuccess.create();
            offlineSaveSuccess.show();

        }
    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }


}

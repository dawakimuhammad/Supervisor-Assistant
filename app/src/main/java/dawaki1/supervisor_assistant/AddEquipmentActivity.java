package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class AddEquipmentActivity extends AppCompatActivity {

    String equipmentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_equipment);

        //add back button on action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //find all button and select boxes from layout
        final EditText equipNameEdtx = findViewById(R.id.equipmentName);
        Button addEquipmentBtn = findViewById(R.id.addEquipment);

        //checks if add category is clicked
        addEquipmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the values entered from the textfield and save in Global variables
                equipmentName = equipNameEdtx.getText().toString();

                if(!equipmentName.equals("")){
                    //add new category to database and notify the user
                    addEquipment();

                    //Reset form for new entry
                    equipNameEdtx.setText("");

                }
                else{
                   Toast.makeText(getApplicationContext(), "All required fields must be completed", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * This method add new eqipment to the database
     */

    public  void addEquipment(){

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ADD_NEW_ITEM,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        if(response.equals("ok")){

                            Toast.makeText(getApplicationContext(), "New equipment added successfully", Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Unable to add new equipment ,please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Check your internet connectivity",Toast.LENGTH_SHORT).show();

            }
        })
        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("equipment_name",equipmentName);
                params.put("item_id","equipment_id");

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);

    }


    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String login_status  = loginStatus.getString(ApiConstants.LOGIN_STATUS,"out");

        if(login_status.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }
}

package dawaki1.supervisor_assistant;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText edtxtUserName, edtxPassword;
    Button signIn;
    public SharedPreferences loginStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        edtxtUserName =  findViewById(R.id.edtUsername);
        edtxPassword = findViewById(R.id.edtPassword);
        TextView forgetPassLink = findViewById(R.id.txtForgotPass);

        forgetPassLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPasswordDialog();
            }
        });


        signIn =  findViewById(R.id.btnSignIn);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginUser (edtxtUserName.getText().toString(),edtxPassword.getText().toString());

            }
        });
    }

    /**
     * this checks the user details beofre granting him access to the application
     * @param username
     * @param password
     */

    private void loginUser (final String username, final String password){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.LOGIN_URL,
                new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject.getString("reqStatus").equalsIgnoreCase("ok")){

                                JSONArray rows = jsonObject.getJSONArray("result");
                                JSONObject singleRow = rows.getJSONObject(0);

                                loginStatus = getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = loginStatus.edit();

                                editor.putString(ApiConstants.SUPERV_ID_PREF,singleRow.getString("bioId"));
                                editor.putString(ApiConstants.LOT_ID_PREF,singleRow.getString("lotID"));
                                editor.putString(ApiConstants.USERNAME_PREF,username);
                                editor.putString(ApiConstants.PASSWORD_PREF,password);
                                editor.putString(ApiConstants.LOGIN_STATUS,"in");

                                editor.commit();

                                Intent fromLogin = null;
                                if(singleRow.getString("accessLvl").equals("AL1")){//AL1 means super aministrator
                                    fromLogin = new Intent(LoginActivity.this,AdminMenuActivity.class);
                                }
                                else {
                                    //this means its supervisor
                                    fromLogin = new Intent(LoginActivity.this,MenuActivity.class);
                                }
                                startActivity(fromLogin);

                            }
                            else {
                                Toast.makeText(getApplicationContext(),jsonObject.getString("result"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Unable to connect, switch on your internet",Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("username",username);
                params.put("password",password);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //show forggt message dialog box to retrieve password
    private Dialog forgotPasswordDialog() {

        final LayoutInflater inflater = this.getLayoutInflater();
        final AlertDialog.Builder confirmDialogue = new AlertDialog.Builder(LoginActivity.this);
        final View customDialogueView = inflater.inflate(R.layout.forgotpassword,null);
        confirmDialogue.setView(customDialogueView)
                // Add action buttons

                .setTitle("Forgot Password")
                .setPositiveButton("Retrieve", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        //check password here
                        //sendEmail();

                    }
                })
                .setNegativeButton("Cancel",  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        confirmDialogue.create();
        return confirmDialogue.show();


    }

}

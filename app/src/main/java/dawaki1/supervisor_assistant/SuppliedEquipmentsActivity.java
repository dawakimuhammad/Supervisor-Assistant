package dawaki1.supervisor_assistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This calss is for supervisor to view equipments supplied to him
 */

public class SuppliedEquipmentsActivity extends AppCompatActivity {

    static ArrayList<String> equipmentNameArr,equipmentIdArr = new ArrayList<>();
    Spinner equipmentSpinner;
    SharedPreferences loginStatus;
    String equipId, quantity,supervisorId,lotIdPref;
    EditText qauntityEdtx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.supplied_equipments);

        //display back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        lotIdPref = loginStatus.getString(ApiConstants.LOT_ID_PREF,"1");
        supervisorId = loginStatus.getString(ApiConstants.USERNAME_PREF,"dawaki@live.com");

        equipmentSpinner = findViewById(R.id.spinnerEquipTypes);
        qauntityEdtx = findViewById(R.id.quantitySup);
        Button saveSupplyRecord = findViewById(R.id.saveSupplyRecord);

        //call to function and display list
        fetchEquipments();

        //Get item selected from the spinner
        equipmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                equipId = equipmentIdArr.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        saveSupplyRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity = qauntityEdtx.getText().toString();
                if(!quantity.equals("")) {
                    saveEquipmentRecord(supervisorId, lotIdPref, quantity, equipId);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Error: You must input the quantity supplied", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
    //get all equipmenst supplied to the supervisor
    private  void fetchEquipments(){

        SuppliedEquipmentsActivity.equipmentIdArr = new ArrayList<>();
        SuppliedEquipmentsActivity.equipmentNameArr = new ArrayList<>();

        //send post request to the server
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_SPINNER_OPTIONS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject.getString("reqStatus").equals("ok")) {

                        JSONArray rows = jsonObject.getJSONArray("result");

                        //populate the lot spinner adapter arraylist
                        for(int i=0;i<rows.length();i++){

                            JSONObject singleRow = rows.getJSONObject(i);
                            //save the valu and name for display in the spinner view
                            SuppliedEquipmentsActivity.equipmentIdArr.add(singleRow.getString("equipmentId"));
                            SuppliedEquipmentsActivity.equipmentNameArr.add(singleRow.getString("equipmentName"));
                        }

                    }
                    else{
                        //store 0 to show spinner has no value
                        SuppliedEquipmentsActivity.equipmentIdArr.add("0");
                        SuppliedEquipmentsActivity.equipmentNameArr.add(jsonObject.getString("result"));

                    }


                } catch (JSONException e) {

                    Toast.makeText(getApplicationContext(), "No equipment found, (spinner items)", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();

                }
                //populate the list view
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.custom_spinner_item,equipmentNameArr);

                adapter.setDropDownViewResource(R.layout.custom_spinner_item);
                equipmentSpinner.setAdapter(adapter);

            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("fetch_id","equipment_id");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
    public void saveEquipmentRecord(final String supervUsername,final String lotId,final String equipQuanity,final String equipId){

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ADD_NEW_ITEM,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        if (response.equals("ok")) {
                            Toast.makeText(getApplicationContext(), "Record of tools saved successfully", Toast.LENGTH_SHORT).show();
                            qauntityEdtx.setText("");
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Unable to save equipment record, please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Check your internet connectivity", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("item_id", "received_equipment");
                params.put("supervUsername", supervUsername);
                params.put("lot_id", lotId);
                params.put("equip_id", equipId);
                params.put("equip_quantity", equipQuanity);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);

    }


    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }

}

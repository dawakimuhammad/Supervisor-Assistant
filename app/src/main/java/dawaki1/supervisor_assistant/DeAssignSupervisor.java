package dawaki1.supervisor_assistant;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The class helps the super admin to revoke the role of supervisors from employees
 */

public class DeAssignSupervisor extends AppCompatActivity {

    private static ArrayList<String>  lotIdArr,lotNames,supervisorNames,supervisorIds = new ArrayList<String>();
    SharedPreferences loginStatus;
    private String superAdminId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.revoke_supervisor);

        //collect the value of the login status
        loginStatus = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        superAdminId = loginStatus.getString(ApiConstants.USERNAME_PREF,"dawaki@live.com");

        //get all list of supervisors
        fetchSupervisors();


    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return supervisorNames.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.custom_list_revoke_employee,null);
            final TextView txtSupervisorName =  convertView.findViewById(R.id.txtSupervNames);
            final TextView txtLotNames =  convertView.findViewById(R.id.txtSupervlotName);
            ImageView btnDelete = convertView.findViewById(R.id.deleteSuperv);


            //populate the list with supervisor information
            txtSupervisorName.setText(supervisorNames.get(position));
            txtLotNames.setText("Lot Name: "+lotNames.get(position).toUpperCase());

            //store the supervisorId and lot id in TAGs which are to be used for deletion
            txtSupervisorName.setTag(supervisorIds.get(position));
            txtLotNames.setTag(lotIdArr.get(position));

            //If the delete icon is clicked, then revoke the role of supervisor
            //attach click listener to delete icon
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //show dialog box to confirm de-approval
                    final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(DeAssignSupervisor.this);

                    alertBuilder.setMessage("Do you really want to revoke the role of this supervisor?")

                            .setTitle("Confirmation")
                            //if yes is selected, then do the action
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    //call the function to de-assign supervisor
                                    //pass supervisorId and lot_id saved into the TAGs line 85 and 86
                                    revokeSupervisorRole(txtSupervisorName.getTag().toString(),txtLotNames.getTag().toString());
                                    dialog.cancel();

                                }
                            })
                            //if no is selected, then do nothing

                            .setNegativeButton("Cancel",  new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    alertBuilder.create();
                    alertBuilder.show();


                }
            });

            return convertView;

        }
    }

    /**
     * This method send request to get all the list of active supervisors from the database
     */
    private  void fetchSupervisors(){

        if(ApiConstants.isInternetAvailable(DeAssignSupervisor.this)) {

            //initiate all the listviews
            DeAssignSupervisor.lotIdArr = new ArrayList<>();
            DeAssignSupervisor.lotNames = new ArrayList<>();
            DeAssignSupervisor.supervisorNames = new ArrayList<>();
            DeAssignSupervisor.supervisorIds = new ArrayList<>();

            //send request to the server to fecth all list of supervisors
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.GET_ALL_SUPERVISORS, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        if (jsonObject.getString("reqStatus").equals("ok")) {

                            //decode and store the jSon record to an array
                            JSONArray rows = jsonObject.getJSONArray("result");

                            for (int i = 0; i < rows.length(); i++) {

                                JSONObject singleRow = rows.getJSONObject(i);
                                //save seperate record to the  arrraylist for display
                                DeAssignSupervisor.supervisorNames.add(singleRow.getString("lName").toUpperCase() + ", " + singleRow.getString("fName"));
                                DeAssignSupervisor.supervisorIds.add(singleRow.getString("supervisor_id"));
                                DeAssignSupervisor.lotIdArr.add(singleRow.getString("lotIdDB"));
                                DeAssignSupervisor.lotNames.add(singleRow.getString("lotName"));

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("result"), Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ListView plotOptions = findViewById(R.id.supervisorsList);
                    DeAssignSupervisor.CustomAdapter customAdapter = new DeAssignSupervisor.CustomAdapter();
                    plotOptions.setAdapter(customAdapter);


                }
            },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(getApplicationContext(), "Internet connection not available, please connect and try again", Toast.LENGTH_SHORT).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("superAdminId", superAdminId);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
        else{

            final AlertDialog.Builder alertBuilder1 = new AlertDialog.Builder(DeAssignSupervisor.this);

            alertBuilder1.setMessage("The list of Supervisors cannot be displayed due to lack of INTERNET connection, please connect your device and try again")
                    // Add action buttons
                    .setTitle("Internet Connection")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //dismiss the dialog box
                            dialog.cancel();

                        }
                    });

            alertBuilder1.create();
            alertBuilder1.show();

        }
    }



    /**
     * When this method is called by passing the supervisor Id, it de-activates him completely
     * @param supervisorID
     * @param lotID
     */

    private void revokeSupervisorRole(final String supervisorID, final String lotID){

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ApiConstants.ADD_NEW_ITEM,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        if(response.equals("ok")){

                            //refresh the screen and show success message
                            fetchSupervisors();
                            Toast.makeText(getApplicationContext(), "Supervisor suspended successfully", Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Unable to de-assign supervisor ,please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Check your internet connectivity",Toast.LENGTH_SHORT).show();

            }
        })
        {

            @Override
            protected Map<String, String> getParams() {

                //post parameters to database
                Map<String, String> params = new HashMap<>();
                params.put("supervisorID",supervisorID);
                params.put("lotID",lotID);
                params.put("item_id","revoke_supervisor");

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);


    }

    /**
     * The method below ensures that user did not have access to pages
     * if he logs out and then press back button which will bring back the activity
     */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Status = getApplicationContext().getSharedPreferences(ApiConstants.LOGIN_PREF_DATA, Context.MODE_PRIVATE);
        String loginStatus  = Status.getString(ApiConstants.LOGIN_STATUS,"out");

        if(loginStatus.equals("out")){

            Intent logOUT = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(logOUT);

        }
    }


}
